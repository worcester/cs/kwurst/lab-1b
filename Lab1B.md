*Version 2021-Spring-1.0, Revised 28 January 2021*

### *CS-140 &mdash; Spring 2021*

# Laboratory 1B

## Objectives

The objective of this lab is to have students successfully set up their computer for submitting their Java programs, to share and collaborate with their lab partners, and to receive feedback on their programs from the instructor.

First, the students will install Git and use it to retrieve the course repository that will be used for submitting their work to the instructor.

Once students have their computer appropriately configured, they will be prepared to develop Java software for future assignments and submit it to the instructor.

Finally, students will enter a simple Java program, compile that program, and run it. Then they will share it with their lab partner, and submit it to the instructor.

## Part 1 &mdash; Do this on your computer *and* your partner’s computer(s)

### 1. Continue to work with your partner(s) for Lab 1A

For the first part of this lab, you should set up both of your computers with the software. Work together to be sure that each person can get the software installed on their own computer.

Ask the instructor if you are having problems that you cannot solve together.

### 2. Create Your GitLab Account

1. Open your browser and go to [https://gitlab.com/users/sign_up](https://gitlab.com/users/sign_up)
2. Create an account.
3. **One you have completed the account creation process, let your instructor know what username you chose.** You will have to be added to a group for the course before you can work on Part 2.

<!-- 4.	Add your SSH key:
    1.	Go to the *Settings* by clicking on the dropdown with the circle at the top right of the screen. (That circle can hold a picture of you if you set an avatar under *Settings*")
    2.	Click on the *SSH Keys* in the menu at the top center of the screen.
    3.	Copy your public key to the clipboard.
		
        1.	Go back to your terminal window.
		
        2.	On Windows type: `notepad ~/.ssh/id_rsa.pub`
			
            1.	In the Notepad window, select all the text and copy it.
		
        3.	On Mac OS X, type: `pbcopy < ~/.ssh/id_rsa.pub`
        4.	Switch back to your browser and paste your key into the key box.
    4.	Type a title for your key. (It can be anything you want.)
    5.	Click the green *Add Key* button.
    6.	If you are on Windows, close Notepad.
 -->

### 3. Install Git

1. In your browser and go to: [https://git-scm.com/downloads](https://git-scm.com/downloads)
2. The picture of the monitor on the page should show the download for your operating system. Click on the download button and save the installer.
3. Run the installer.
4. On Windows, use the default settings, when given choices.
5. Start a command-line/terminal window, as shown below:
    1. On Windows, go to the Start menu > All Programs > Git > Git Bash
    2. On Mac OS X, go to the Applications folder > Utilities folder > Terminal
6. Configure Git, and so that it knows who you are and can label your changes. Type the following commands (replacing *`Jane Smith`* and *`jsmith@worcester.edu`* with your own name and email address):

    ```bash
    git config --global user.name "Jane Smith"
    git config --global user.email jsmith@worcester.edu
    git config --global init.defaultBranch main
    ```

>The last line tells Git what it should name the default branch if you create a project on your computer. This will not be so important in this class, but may be for you in future classes that use Git.)

<!-- ### 3. Create an SSH Key Pair
SSH (Secure Shell) is a network protocol for encrypted data transmission. It is often used for password-less login to remote servers, and we will be using it to allow you to communicate with the GitLab server without having to type your password every time you want to send code changes to the server. SSH uses public-key encryption, and to use it you need to create a *public-private key pair*. Your public key is made available to anyone with whom you want to exchange encrypted data. Your private key is used only on your computer and should be protected carefully.

1.	Create an ssh key pair by typing: <br>

    ```bash
    ssh-keygen -t rsa -C jsmith@worcester.edu
    ```
    1. When it asks you for the filename, use the default, by hitting `Enter`.
    2.	You should enter a passphrase when prompted. Using a passphrase will assure that no one else can use your private key, even if they gain access to the key file. **When you type your passphrase, your typing will not show &mdash; not even as &bull; or \*.**

2.	Keep your Git Bash or Terminal window open. -->


## Part 2 &mdash; Work on *one* computer and *one* GitLab account

Choose one person’s computer and that same person’s GitLab account for this section. That person will be the owner of the repository for this lab. (The other partner(s) will be able to contribute to the repository, but there can be only one owner.)

### 4. Copy (fork) the Lab1B Repository

The materials created for this lab are stored in a Git *repository*. A repository (also called a project in GitLab) is a directory tree containing all the files and directories being used for a particular project and the history of changes made to them. The Lab1B repository is stored on GitLab and is shared with a group created for this class. As a member of this class, you have read-only access to this repository. For you to be able to make changes to the files, you must make your own copy of the repository. To do this you must *fork* the repository. This will make a new repository that you own, with copies of all the files and history in the original. (Refer to Model 2 in Activity 1)

1. In your browser, go to the `CS 140 OL OL2 OL3 Spring 2020` group in GitLab: [https://gitlab.com/worcester/cs/cs-140-ol-ol2-ol3-spring-2020](https://gitlab.com/worcester/cs/cs-140-ol-ol2-ol3-spring-2020)
2. Click on the Lab1B project.
3. You will notice that this project is listed as Private (note the lock icon to the right of the project name.) For you to be able to make any changes to the project, you need your own copy. You will do that by *forking* the project:
    1. Click on the *Fork* button to the right of the project name.
    2. Choose your account to fork the project to.
    3. Once the project is successfully forked, you will notice that the project is still private, but now the project is under your name, rather than the class group.
4. Add your instructor and your partner(s) as members of your repository.
    1. In the left menu select *Members*.
    2. In the *Invite member* box, enter your instructor's username (`kwurst` or `ebraynova` or `nalsallami`)
        1. Choose the correct account from the drop-down.
    3. Repeat the step above to enter the username(s) for your partner(s) (You can enter more than one name in the box.)
    4. In the dropdown below that, select `Maintainer`
    5. Click the *Invite* button
5. Return to the *Project* page by clicking *Lab 1B* in the left menu.

### 5. Download (clone) the Lab1B Repository

Now you have a copy of the Lab1B repository in your account on GitLab, but you must download it to your computer before you can make any changes. To this you must *clone* the remote repository (on GitLab) to produce a local repository (on your computer.) (Refer to Model 3 in Activity 1)

In Lab1A you created a folder to hold your work for this course. We are going to navigate to this folder and clone the project there.

1. Return to your Git Bash or Terminal window.
2. Navigate to your course folder. Refer to Activity 3 (particularly Model 5). *(See note at the bottom of this lab for easier ways to do this.)*
3. Copy the project URL from the `Clone` box to the right the star button.
    1. Make sure that use the `Clone with HTTPS` option.
    2. It will look like: `https://gitlab.com:jsmith/lab-1b.git`
4. In your Git Bash or Terminal window, in your course folder, type the following command to clone the Lab1 repository into a `Lab1B` folder (using the URL you copied earlier):

    ```bash
    git clone https://gitlab.com:jsmith/lab-1b.git Lab1B
    ```

    1. You may be asked to enter “yes” to allow a connection to the server the first time you do this. 
    2. You will have to enter the password you created for your GitLab account.
    3. You will see output that looks similar to this:

    ```bash
    Cloning into 'Lab1B'...
    remote: Counting objects: 6, done.
    remote: Compressing objects: 100% (4/4), done.
    remote: Total 6 (delta 0), reused 0 (delta 0)
    Unpacking objects: 100% (6/6), done.
    ```

5. Navigate to the newly created `Lab1B` directory.
6. The `Lab1B` folder on the selected computer is the local repository, the one on GitLab is the *remote repository* and the *origin* of the local repository. Check the remotes for your repository by typing the following command:

    ```bash
    git remote –v
    ```

    Your remote repository is listed twice (once as fetch and once as push).

    ```bash
    origin      https://gitlab.com:jsmith/lab-1b.git (fetch)
    origin      https://gitlab.com:jsmith/lab-1b.git (push)
    ```

### 6. Study the Sample Output and the Skeleton Code

1. Start BlueJ.
2. From the Project menu, choose Open Project…
3. Navigate to your Lab1B folder, select the Lab1BCode folder and click the Open button.
4. You will have one class named NamePrinter with three empty method bodies.

### 7. Complete the Code for `printName_1` and `main` Methods

Whichever group member owns the computer you are working on should do the typing for the following. Their partner(s) should read instructions and help.

1. Study the output and the method comments.
2. Modify the `@author` and `@version`.
    1. Replace `(your name)` with your actual name. *Do not leave the parentheses around your name.*
    2. Replace `(version)` with `1`. *Do not leave the parentheses around the version number.*
3. Modify the  method `printName_1` to draw the first name or last name of the person typing.
    1. See `Output.png` for an example.
4. Modify the `main` method to call the `printName_1` method by adding the following code (before the comment):

    ```java
    printName_1();
    ```

### 8. Commit Your Working Program

We are now going to use Git to save (*commit*) your working program for two reasons:

* So that you can make changes to your program, and come back to this point if you want/need to.
* So that you can share your program with your partner and with the instructor.

When you use Git (and in programming in general), you should work in small iterations:

* Add some new, small feature to a working (committed) program.
* When that feature works, commit the changes (with a useful message so you can remember what it did at that point).
* Repeat the process until you’ve added everything your program needs to do.

If, at any point, you break your program so completely that you can’t fix it &mdash; revert back to a previous, working commit. Think about another way to attack your latest problem (maybe get help from someone else) and try again!

Even the most experienced programmers have these kinds of problems, which is why version control was invented.

1. Return to your Git Bash or Terminal window, and be sure you are still in the Lab1 directory.
2. Type the command `git status`

    ```bash
    On branch main
    Your branch is up to date with 'origin/main'.

    Changes not staged for commit:
      (use "git add <file>..." to update what will be committed)
      (use "git restore <file>..." to discard changes in working directory)

            modified:   Lab1Code/NamePrinter.java
            modified:   Lab1Code/package.bluej
    ```

The response tells us that there are files that Git realized have changed. *Notice that they are in red on your screen.*

> Notice the use of `main` and `origin/main` above.
>
>* As mentioned in Step 5, `origin` refers to the remote repository that this local repository was cloned from.
>* `main` is the name of the branch that we are currently working on. More advanced programmers may work on different features of a program in separate branches and them merge these all together into a single working program later.
>* Note: Traditionally the default branch in Git has been called `master`. However, the term `master` in Computer Science has become problematic because of past use of the terms `master/slave`. The community is moving toward the use of `main` for the default branch in a Git repository, however you may still see repositories with a `master` branch. (See Resources at the end of this lab for more on Inclusive Language efforts in CS.)

3. We want Git to stage the changes in that file so type the commands:

    ```bash
    git add Lab1Code/NamePrinter.java
    git add Lab1Code/package.bluej
    ```

4. If you type `git status` again, you will see that Git is expecting to commit the files, and will save the changes when you tell it to (commit). *Notice that they are all green now.*

    ```bash
    On branch main
    Your branch is up to date with 'origin/main'.

    Changes to be committed:
      (use "git restore --staged <file>..." to unstage)

            modified:   Lab1Code/NamePrinter.java
            modified:   Lab1Code/package.bluej
    ```

5. The current state of our project (working correctly!) is one that we want to be able to share and to go back to. To do this, we tell Git we want to save the changes (commit) and give it a message to label this state so we can go back here if we want:

    ```bash
    git commit –m'Lab 1 Version 1'
    ```

    Notice the 7-digit number (`48fdfd1`) *it will be different on your computer*. That is the beginning of a unique commit number that we will use to refer to this commit state in future commands.

    ```bash
    [main 48fdfd1] Lab 1 Version 1
    2 files changed,56 insertions(+)
     create mode 100644 Lab1Code/NamePrinter.java
     create mode 100644 Lab1Code/package.bluej
    ```

6. If we check the status again, Git will tell us that all the changes have been saved, and no changes have been made since the last commit:

    ```bash
    git status
    ```

    ```bash
    On branch main
    Your branch is ahead of 'origin/main' by 1 commit.
      (use “git push” to publish your local commits)

    nothing to commit, working directory clean
    ```

7. We can ask Git to show us all the commits that have been made on this project so far: `git log`. Notice that the full commit number is given here (*it will be different on your computer*). Usually the first 7 digits are enough for any command that needs a commit number. Your output will be similar, but not exactly the same.

    ```bash
    commit 48fdfd1cbd41cef6603b4d48d4cf9b8ddfa33c46
    Author: Karl R. Wurst <karl@w-sts.com>
    Date:   Wed May 15 19:03:42 2013 -0400

        Lab 1 Version 1

    commit 3fa3d5083458c4cb455458d73a3972ff1dab25eb
    Author: Karl R. Wurst <karl@w-sts.com>
    Date:   Wed May 15 18:47:02 2013 -0400

        Added README

    commit c6a39f3a85debf025b0484add8607df02639fa1f
    Author: Karl R. Wurst <karl@w-sts.com>
    Date:   Wed May 15 18:45:25 2013 -0400

        Added .gitignore.
    ```

8. If necessary press `q` to get out of the log output.

### 9. Share (push) Your Program with Your Partner(s) and Your Instructor

Now we are going to get the program onto the second computer. (Refer to Model 4 in Activity 1)

1. On the selected computer, push your repository to GitLab:

    ```bash
    git push
    ```

## Part 3 &mdash; Let the other partner(s) try it

Another partner should do the following on their own computer.

### 10. Copy (clone) The Program Onto Your Partner’s Computer

1. Switch to working on the second computer.
2. Return to your Git Bash or Terminal window.
3. Change directory to your course folder.
4.  Once you are in your course folder, type the following command to clone your partner’s Lab1B repository into a Lab1B folder (be sure to replace jsmith your partner’s GitLab username):

    ```bash
    git clone git@gitlab.com:jsmith/lab-1b.git Lab1B
    ```

5. Now you should have a `Lab1` folder that you can open in BlueJ.

### 11. Modifying Your Program

Make changes to your program to do the following:

1. Add another `@author` line with your own name
2. Update the version number.
3. Create a new method `printName_2` to draw the first name or last name of the person typing.
4. Modify the `main` method to call the `printName_2` method by adding the following code:

    ```java
    public static void main (String [] args) {

        printName_1();
        // insert code here to print two blank lines between the two names
        printName_2();

    }
    ```

5. Figure out how to print two blank lines between the names, and add the code. (And remove the comment.)

### 12. Commit Your New Working Program

Follow the steps under Step 8 again to commit the changes to your new working program. A few differences:

1. There will only be one modified file that you will have to tell Git to stage.
2. You will want a different commit message. I suggest `Lab 1 Version 2 with names`
3. You will get a new commit number.
4. When you check the log, you will see your new commit.

### 13. Share (push) Your Program with Your Partner and Your Instructor

Now we are going to get the program onto the second computer. (Refer to Model 5 in Activity 1)

1.	On the selected computer, push your repository to GitLab:

    ```bash
    git push
    ```

### 14. Repeat with a Third Partner (if applicable)

If you have a third partner, have that partner repeat Sections 10, 11, 12, and 13 above on their own computer.

## Part 4 &mdash; Make sure all partners have the most recent version

### 15. Update (pull) the Version on the other Computer(s)

Back on the other computer(s):

1. Get the changes your partner(s) made by pulling them from GitLab:

    ```bash
    git pull
    ```

2. Open the program in BlueJ and make sure you got the changes.
3. In the future, it’s always a good idea make sure you have all the most recent changes to the repository before starting to modify the files. Do this with the `git pull origin main` command.

## Deliverables

The instructor will pull your Lab1B from your GitLab repository to grade it. Make sure:

1. You have pushed all changes to your shared repository. (Your instructor can’t access local changes on your computer.)
2. You have added your instructor as Maintainer to your shared GitLab repository.

## Resources

### More About Version Control

If you want to learn more about version control, you may want to take a look at this article: [http://betterexplained.com/articles/a-visual-guide-to-version-control/](http://betterexplained.com/articles/a-visual-guide-to-version-control/)

### Easier Ways to Open a Folder in the Terminal

#### On Windows

Right-click on the folder that you want to open, and choose `Git Bash here`.

#### On Mac

Install [Go2Shell](https://apps.apple.com/us/app/go2shell/id445770608?mt=12). This will add a button `>_<` to the Finder window that allows you to open the current folder in Terminal.

### Inclusive Language

There has been a push to use more inclusive language in software development. Here are some references:

* Guidance from ACM: [https://www.acm.org/diversity-inclusion/words-matter](https://www.acm.org/diversity-inclusion/words-matter)
* Inclusive Naming Initiative: [https://inclusivenaming.org/](https://inclusivenaming.org/)
* Google’s initiative for more inclusive language in open source projects [https://opensource.googleblog.com/2020/11/googles-initiative-for-more-inclusive.html](https://opensource.googleblog.com/2020/11/googles-initiative-for-more-inclusive.html)

One of the problematic terms is "master" (as in master/slave). The default branch in Git repositories has been master since Git was created. This is changing.

* GitHub has now changed their software to name the default branch "main" in any repositories created in GitHub.
* Git is discussing making the change, but in the meantime has added an option to set the default branch name when you create a new repository (with git init). You can also set your git configuration to set the default branch using the following command: git config --global init.defaultBranch main
* GitLab is discussing the change but has not yet provided any way to set the default name when you create a repository in GitLab.

---
&copy;2021 Karl R. Wurst, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit <a href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank">http://creativecommons.org/licenses/by-sa/4.0/</a> or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
